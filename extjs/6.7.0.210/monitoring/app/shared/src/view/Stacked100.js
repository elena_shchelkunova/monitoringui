/**
 * 100% stacked bars are bar charts where categories are stacked
 * on top of each other. The value of each category is recalculated so that
 * it represents a share of the whole, which is the full stack and is equal
 * to 100 by default.
 */
Ext.define('Monitoring.view.Stacked100', {
    extend: 'Ext.Panel',
    xtype: 'bar-stacked-100',
    controller: 'bar-stacked-100',
    requires: ['Monitoring.store.Browsers'] ,
    // <example>
    // Content between example tags is omitted from code preview.
    bodyStyle: 'background: transparent !important',
    layout: {
        type: 'vbox',
        pack: 'center'
    },
    otherContent: [{
        type: 'Controller',
        path: 'Stacked100Controller.js'
    }, {
        type: 'Store',
        path: 'app/shared/src/store/Browsers.js'
    }],
    // </example>

    width: 650,

    items: [{
        xtype: 'cartesian',
        reference: 'chart',
        width: '100%',
        height: 500,
        captions: {
            title: 'Процент трудоустройства выпускников',
            credits: {
                text: 'Здесь может размещаться \n'+
                    'все, что вы пожалаете',
                align: 'left'
            }
        },
        legend: {
            docked: 'bottom'
        },
        store: {
            type: 'browsers'
        },
        flipXY: true,
        axes: [{
            type: 'numeric',
            fields: 'data1',
            position: 'bottom',
            grid: true,
            minimum: 0,
            maximum: 100,
            majorTickSteps: 10,
            renderer: 'onAxisLabelRender'
        }, {
            type: 'category',
            fields: ['month'],
            position: 'left',
            grid: true
        }],
        series: [{
            type: 'bar',
            fullStack: true,
            title: [ 'Процент трудоустроенных по специальности', 'Процент трудоустроенных не по специальности', '-' ],
            xField: 'month',
            yField: [ 'data1', 'data2', 'data3' ],
            axis: 'bottom',
            stacked: true,
            style: {
                opacity: 0.80
            },
            highlight: {
                fillStyle: 'yellow'
            },
            tooltip: {
                trackMouse: true,
                renderer: 'onSeriesTooltipRender'
            }
        }]
        //<example>
    }],

    tbar: [
        '->',
        {
            text: 'Preview',
            handler: 'onPreview'
        }
    ]
});
