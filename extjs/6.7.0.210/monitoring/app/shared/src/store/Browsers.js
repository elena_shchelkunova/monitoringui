Ext.define('Monitoring.store.Browsers', {
    extend: 'Ext.data.Store',
    alias: 'store.browsers',

    //                   IE    Firefox  Chrome   Safari
    fields: ['month', 'data1', 'data2', 'data3'],

    constructor: function (config) {
        config = config || {};

        config.data = [
            { month: 'Московский Государственный Университет', data1: 20, data2: 37, data3: 35},
            { month: 'Санкт-Петербургский Государственный Университет', data1: 20, data2: 37, data3: 36 },
            { month: 'Московский Институт Международных Отношений', data1: 19, data2: 36, data3: 37 },
            { month: 'Санкт-Петербургский Политехнический Университет', data1: 18, data2: 36, data3: 38 },
            { month: 'Санкт-Петербургский Университет Путей Сообщения', data1: 18, data2: 35, data3: 39 },
            { month: 'Тюменский Государственный Университет', data1: 17, data2: 34, data3: 42 },
            { month: 'Дальневосточный Университет', data1: 16, data2: 34, data3: 43 },
            { month: 'Казанский Государственный Университет', data1: 16, data2: 33, data3: 44 },
            { month: 'Еще и еще ВУЗЫ', data1: 16, data2: 32, data3: 44 },
            { month: 'Еще и еще ВУЗЫ', data1: 16, data2: 32, data3: 45},
            { month: 'Еще и еще ВУЗЫ', data1: 15, data2: 31, data3: 46 },
            { month: 'Еще и еще ВУЗЫ', data1: 15, data2: 31, data3: 47 }
        ];

        this.callParent([config]);
    }

});